<?php
$y = isset($_GET['y']) ? $_GET['y'] : date("Y");

$timeStamp = strtotime($y . "-01-01");
$month = array(
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "11",
    "12"
);

if ($timeStamp === false) {
    $timeStamp = time();
}

// ファイルの呼び出しと配列化、文末文字の無効化
$holidayData = file("pub.txt", FILE_IGNORE_NEW_LINES);

// 当年、当月毎に必要な祝日のみを抽出（参照データを減らすことで処理速度改善）
foreach ($month as $thisMonth) {
    $thisYearHolidays[$thisMonth] = preg_grep("/^$y\/$thisMonth/", $holidayData);
}

// 外部ファイルのテキストデータを破棄してメモリを解放する（メモリの容量オーバー回避）
unset($holidayData);

/**
 * 当月のカレンダー内の日付を取得します。
 * @param $currentMonth 表示するカレンダーの月
 * @param $year         表示するカレンダーの年
 * @param $currentYearHolidays 表示するカレンダーの月の祝日
 *
 */
function getweeks($currentMonth, $year, $currentYearHolidays)
{

    // 月の最終日
    $lastDay = date("t", strtotime($year . "-" . $currentMonth . "-01"));
    // 月の１日の曜日番号
    $youbi = date("w", strtotime($year . "-" . $currentMonth . "-01"));

    $weeks = array();
    $week = '';

    // 月初めの隙間の数
    if ($youbi == 0) {
        $week .= str_repeat('<td></td>', 6);
    } else {
        $week .= str_repeat('<td></td>', $youbi - 1);
    }

    // その月が終わるまで、日付と曜日を１づつ増やしていく
    for ($day = 1; $day <= $lastDay; $day ++, $youbi ++) {

        // 祝日を照らし合わせてクラスを付与
        $now = $year . '/' . $currentMonth . '/' . "$day";
        $publicHoliday = array_search($now, $currentYearHolidays);

        $class = '';
        if ($publicHoliday !== false) {
            $class = "publicHoliday";
        }

        // 日付
        $week .= sprintf('<td class="youbi_%d %s">%d</td>', $youbi % 7, $class, $day);
        if ($youbi % 7 == 0 || $day == $lastDay) {
            if ($day == $lastDay) {

                // 終わりの隙間
                if ($youbi % 7 == 0) {
                    $week .= '';
                } else {
                    $week .= str_repeat('<td></td>', 7 - ($youbi % 7));
                }
            }
            // 週ごとに行を変える
            $weeks[] = '<tr>' . $week . '</tr>';
            $week = '';
        }
    }
    return $weeks;
}

// 前年ページと翌年ページを作る
$prev = date("Y", strtotime($y - 1 . "-01-01"));
$next = date("Y", strtotime($y + 1 . "-01-01"));

?><!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>PHPカレンダー</title>
<link rel="stylesheet" href="P85calendar0507.css">
</head>
<body>
    <header>
        <p class="years">
            <!-- 前年、翌年へのリンクの設置 -->
            <a href="?y=<?php echo $prev; ?>">&laquo;</a>
<?php echo $y; ?>
            <a href="?y=<?php echo $next; ?>">&raquo;</a>
        </p>
    </header>
    <ul>
<?php
foreach ($month as $thisMonth) {
?>
        <li>
            <table>
                <thead>
                    <tr>
                        <th colspan="7">
<?php
    echo $thisMonth;
?>
                        </th>
                    </tr>
                    <tr>
                        <th>Mo</th>
                        <th>Tu</th>
                        <th>We</th>
                        <th>Th</th>
                        <th>Fr</th>
                        <th>Sa</th>
                        <th>Su</th>
                    </tr>
                </thead>
                <tbody>
<?php
    $weeks = getweeks($thisMonth, $y, $thisYearHolidays[$thisMonth]);
    foreach ($weeks as $weekOutPut) {
        echo $weekOutPut;
    }
?>
                </tbody>
            </table>
        </li>
<?php
}
?>
    </ul>
</body>
</html>
